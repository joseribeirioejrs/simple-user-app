import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IUser } from './user.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getUser(id) {
    return this.http.get(`${environment.users}/${id}`);
  }

  getUsers() {
    return this.http.get(environment.users);
  }

  newUser(user: IUser) {
    return this.http.post(environment.users, user);
  }

  updateUser(user: IUser) {
    return this.http.put(`${environment.users}/${user.id}`, user);
  }

  deleteUser(id: number) {
    return this.http.delete(`${environment.users}/${id}`);
  }
}
