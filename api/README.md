Esse é apenas uma simulação de API usando JSON SERVER

Para rodar basta executar o seguinte comando

```
json-server --watch db.json
```

Foi usado o site 4devs para gerar os mocks

https://www.4devs.com.br/gerador_de_pessoas