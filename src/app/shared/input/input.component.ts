import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { v4 as uuidv4 } from 'uuid';

export type InputType = 'text' | 'number' | 'email' | 'password';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InputComponent),
    multi: true
  }]
})
export class InputComponent implements OnInit, ControlValueAccessor {
  id = uuidv4();
  val = '';
  @Input() label = '';
  @Input() type: InputType = 'text';
  @Input() errorMessage = 'Ops, algo está errado';
  @Input() mask = '';
  @Input() isInvalid = false;
  @Input('value') get value(): any {
    return this.val;
  }
  set value(val) {
    this.val = val;
    this.onChange(val);
    this.onTouch(val);
  }
  constructor() { }

  ngOnInit(): void {
  }

  onChange: any = () => {};
  onTouch: any = () => {};
  writeValue(value: any) {
    this.value = value;
  }
  registerOnChange(fn: any) {
    this.onChange = fn;
  }
  registerOnTouched(fn: any) {
    this.onTouch = fn;
  }
}
