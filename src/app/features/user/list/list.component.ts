import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { IUser } from '../user.interface';
import { CPF_MASK } from '../user.contant';
import { Router } from '@angular/router';
import { Loading } from 'src/app/shared/classes/loading';
import { ErrorStatus } from 'src/app/shared/classes/error-status';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  users: IUser[] = [];
  cpfMask = CPF_MASK;
  errorStatus = new ErrorStatus();
  loadingStatus = new Loading();

  constructor(
    private router: Router,
    private userService: UserService) { }

  ngOnInit(): void {
    this.getUsers();
  }

  getUsers() {
    this.loadingStatus.enableLoading();
    this.userService
      .getUsers()
      .subscribe((users: IUser[]) => {
        this.successGetUsers(users);
      }, error => {
        this.errorRequest();
      });
  }

  successGetUsers(users: IUser[]) {
    this.users = users;
    this.errorStatus.disableError();
    this.loadingStatus.disableLoading();
  }

  errorRequest() {
    this.errorStatus.enableError();
    this.loadingStatus.disableLoading();
  }

  deleteUser(id: number) {
    this.loadingStatus.enableLoading();
    this.userService
      .deleteUser(id)
      .subscribe(() => {
        this.successDeleteUser();
      }, error => {
        this.errorRequest();
      });
  }

  successDeleteUser() {
    this.getUsers();
    this.errorStatus.disableError();
    this.loadingStatus.disableLoading();
  }

  goToNewUser() {
    this.router.navigate(['users', 'new']);
  }

  goToEditUser(id: number) {
    this.router.navigate(['users', 'edit', id]);
  }

}
