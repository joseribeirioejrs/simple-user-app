export class Loading {
    isLoading = false;

    enableLoading() {
        this.isLoading = true;
    }

    disableLoading() {
        this.isLoading = false;
    }
}
