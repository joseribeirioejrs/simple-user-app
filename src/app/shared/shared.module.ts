import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { RouterModule } from '@angular/router';
import { FloatButtonComponent } from './float-button/float-button.component';
import { InputComponent } from './input/input.component';
import { FormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  declarations: [NavbarComponent, FloatButtonComponent, InputComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NgxMaskModule
  ],
  exports: [NavbarComponent, FloatButtonComponent, InputComponent],
})
export class SharedModule { }
