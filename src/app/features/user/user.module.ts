import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { UserRoutingModule } from './user-routing.module';
import { UserService } from './user.service';
import { NgxMaskModule } from 'ngx-mask';
import { NewComponent } from './new/new.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ListComponent, NewComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    NgxMaskModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    UserService,
  ]
})
export class UserModule { }
