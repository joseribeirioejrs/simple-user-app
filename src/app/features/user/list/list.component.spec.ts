import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterModule } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { ListComponent } from './list.component';
import { MOCK_USERS } from '../users.mock';

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListComponent ],
      imports: [
        RouterModule.forRoot([]),
        HttpClientTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('testing successGetUsers', () => {
    spyOn(component.errorStatus, 'disableError');
    spyOn(component.loadingStatus, 'disableLoading');
    component.successGetUsers(MOCK_USERS);
    expect(component.users).toEqual(MOCK_USERS);
    expect(component.errorStatus.disableError).toHaveBeenCalled();
    expect(component.loadingStatus.disableLoading).toHaveBeenCalled();
  });

  it('testing errorRequest', () => {
    spyOn(component.errorStatus, 'enableError');
    spyOn(component.loadingStatus, 'disableLoading');
    component.errorRequest();
    expect(component.errorStatus.enableError).toHaveBeenCalled();
    expect(component.loadingStatus.disableLoading).toHaveBeenCalled();
  });

  it('successDeleteUser', () => {
    spyOn(component, 'getUsers');
    spyOn(component.errorStatus, 'disableError');
    spyOn(component.loadingStatus, 'disableLoading');
    component.successDeleteUser();
    expect(component.getUsers).toHaveBeenCalled();
    expect(component.errorStatus.disableError).toHaveBeenCalled();
    expect(component.loadingStatus.disableLoading).toHaveBeenCalled();
  });
});
