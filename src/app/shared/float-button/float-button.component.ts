import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

export type FloatButtonType = 'primary' | 'secondary' | 'success' | 'danger' | 'warning'
  | 'info' | 'light' | 'dark' | 'link';

@Component({
  selector: 'app-float-button',
  templateUrl: './float-button.component.html',
  styleUrls: ['./float-button.component.scss']
})
export class FloatButtonComponent implements OnInit {

  @Input() type: FloatButtonType = 'primary';
  constructor() { }

  ngOnInit(): void {
  }

  get typeButton() {
    return `btn btn-${this.type} float-button`;
  }
}
