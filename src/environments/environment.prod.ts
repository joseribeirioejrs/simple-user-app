const BASE_URL = 'http://localhost:3000/';

export const environment = {
  production: true,
  users: `${BASE_URL}users`
};
