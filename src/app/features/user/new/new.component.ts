import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { UF } from "src/app/shared/constants/uf";
import { ValidatorsService } from "src/app/shared/validators/validators.service";
import { v4 as uuidv4 } from "uuid";
import { IUser } from "../user.interface";
import { UserService } from "../user.service";
import { Subject, Subscription } from "rxjs";

@Component({
  selector: "app-new",
  templateUrl: "./new.component.html",
  styleUrls: ["./new.component.scss"],
})
export class NewComponent implements OnInit {
  form: FormGroup;
  user: IUser;
  isLoading = false;
  isIntactForm = true;
  UFSelect = UF;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private activatedRouter: ActivatedRoute,
    private validatorsService: ValidatorsService,
    private router: Router
  ) {
    this.form = this.fb.group({
      id: uuidv4(),
      nome: ["", Validators.required],
      cpf: [
        "",
        { validators: [Validators.required, this.validatorsService.cpf] },
      ],
      cep: ["", Validators.required],
      logradouro: ["", Validators.required],
      bairro: ["", Validators.required],
      localidade: ["", Validators.required],
      numero: ["", Validators.required],
      uf: ["", Validators.required],
    });
  }

  ngOnInit(): void {
    if (this.isEditPage) {
      this.getUser();
    }
  }

  get isEditPage() {
    let isEdit = false;
    this.activatedRouter.url.subscribe((URL) => {
      isEdit = URL[0].path === "edit";
    });
    return isEdit;
  }

  get id() {
    const { snapshot } = this.activatedRouter;
    return snapshot?.params?.id;
  }

  enableLoading() {
    this.isLoading = true;
  }

  disableLoading() {
    this.isLoading = false;
  }

  newUser() {
    this.enableLoading();
    this.isIntactForm = false;
    if (this.form.valid) {
      const request = this.isEditPage
        ? this.userService.updateUser(this.form.value)
        : this.userService.newUser(this.form.value);

      request.subscribe(
        () => {
          this.goList();
          this.disableLoading();
        },
        (error) => {
          console.error(error);
          this.disableLoading();
        }
      );
    } else {
      this.disableLoading();
    }
  }

  getUser() {
    this.enableLoading();
    this.userService.getUser(this.id).subscribe(
      (user: IUser) => {
        this.user = user;
        this.setForm(user);
        this.disableLoading();
      },
      (error) => {
        console.log(error);
        this.disableLoading();
      }
    );
  }

  setForm(user: IUser) {
    const keys = Object.keys(user);
    keys.forEach((key) => {
      this.form.controls[key].setValue(user[key]);
    });
  }

  goList() {
    this.router.navigate(["users", "list"]);
  }

  isValidInput(key: string) {
    const { controls } = this.form;

    return (
      controls &&
      controls[key] &&
      controls[key].invalid &&
      !this.isIntactForm
    );
  }

}
