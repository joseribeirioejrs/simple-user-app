import { TestBed } from '@angular/core/testing';
import { FormControl } from '@angular/forms';

import { ValidatorsService } from './validators.service';

describe('ValidatorsService', () => {
  let service: ValidatorsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ValidatorsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('testing correct CPF', () => {
    const CPF = new FormControl('12345678909');
    service = TestBed.inject(ValidatorsService);
    expect(null).toBe(service.cpf(CPF));
  });

  it('testing incorrect CPF', () => {
    const CPF = new FormControl('12345678908');
    const expectResult = { invalidCPF: true };
    service = TestBed.inject(ValidatorsService);
    expect(service.cpf(CPF)).toEqual(expectResult);
  });
});
