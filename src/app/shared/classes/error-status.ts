export class ErrorStatus {
    hasError = false;
    enableError() {
        this.hasError = true;
    }

    disableError() {
        this.hasError = false;
    }
}
