export const MOCK_USERS = [{
    id: 1,
    nome: 'Antonio Ricardo Nunes',
    cpf: '12345678909',
    cep: '38280000',
    logradouro: 'Rua A nº 297',
    bairro: 'Iturama I',
    localidade: 'Iturama',
    uf: 'MG'
  },
  {
    id: 2,
    nome: 'Francisca Isabel Alícia Porto',
    cpf: '53125781698',
    cep: '56310540',
    logradouro: 'Rua Pedro Dias de Araújo, nº 454',
    bairro: 'COHAB Massangano',
    localidade: 'Petrolina',
    uf: 'PE'
}];
