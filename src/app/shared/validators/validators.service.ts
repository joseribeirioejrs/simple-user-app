import { Injectable } from '@angular/core';
import { FormControl, ValidationErrors } from '@angular/forms';
import { validate as validateCPF } from 'gerador-validador-cpf';

@Injectable({
  providedIn: 'root'
})
export class ValidatorsService {

  constructor() { }

  public cpf(control: FormControl): ValidationErrors | null {
    console.log('CONTROL: ', control);
    const isValid = validateCPF(control.value);
    return !!isValid ? null : { invalidCPF: true };
  }

}
